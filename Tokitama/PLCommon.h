//
//  PLCommon.h
//  NoForgetBuy
//
//  Created by Osamu Suzuki on 2015/01/02.
//  Copyright (c) 2014年 Plegineer, Inc. All rights reserved.
//

#ifdef DEBUG
#  define DEBUGLOG(...) NSLog(__VA_ARGS__)
#  define LOG_CURRENT_METHOD NSLog(@"[%@] # %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd))
#else
#  define DEBUGLOG(...) ;
#  define LOG_CURRENT_METHOD ;
#endif

#define HEXCOLOR(c) [UIColor colorWithRed:((c>>16)&0xFF)/255.0 green:((c>>8)&0xFF)/255.0 blue:(c&0xFF)/255.0 alpha:1.0]

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


//TODO:申請時
//サーバーのversion.jsonのバージョンを現状AppStoreに公開されてるバージョン(初の場合は0.0.0)にする。フラグも1にする。

//TODO:申請とおったら
//サーバーのversion.jsonのバージョンを現状AppStoreに公開されてるバージョン(初の場合は1.0.0)にする。フラグも0にする。

#define URL_APP_STORE       @"https://itunes.apple.com/jp/app/id955132078?mt=8"

#define URL_SUPPORT_TWITTER    @"https://mobile.twitter.com/s_0samu"
#define SUPPORT_MAIL           @"app+noforgetbuy@suzukicreative.com"
#define URL_APP_STORE_OYAJINEK @"https://itunes.apple.com/jp/app/id904134254?mt=8"
#define URL_APP_STORE_UNLOCKER @"https://itunes.apple.com/jp/app/id875225343?mt=8"
#define URL_APP_STORE_MEMO     @"https://itunes.apple.com/jp/app/id896419388?mt=8"
#define URL_APP_STORE_CALENDER @"https://itunes.apple.com/jp/app/id908861866?mt=8"
#define URL_APP_STORE_MESSAGE  @"https://itunes.apple.com/jp/app/id931812704?mt=8"
#define URL_APP_STORE_ALL      @"http://appstore.com/suzukicreative"

#define URL_REVIEW_API @"http://plegineer.co.jp/api/noforgetbuy/version.json"

//i-mobile
#define AD_IMOBILE_PUBLISHER_ID   @"29411"
#define AD_IMOBILE_MEDIA_ID       @"137322"//test:129680
#define AD_IMOBILE_SPOT_ID_BANNER1 @"348846"//test:323502
#define AD_IMOBILE_SPOT_ID_ICON @"348894"

//40degreeのやーつ
//#define AD_IMOBILE_PUBLISHER_ID @"29411"
//#define AD_IMOBILE_MEDIA_ID     @"106566"
//#define AD_IMOBILE_SPOT_ID_INT  @"243725"//インタースティシャル
//#define AD_IMOBILE_SPOT_ID_BANNER1 @"243706"//バナー
//#define AD_IMOBILE_SPOT_ID_ICON   @"252517"

//NEND
//#define AD_NEND_APIKEY_BANNER_WEB        @"0e89e16441e04df391f8cc10d6bdb2238584c3b7"
//#define AD_NEND_SPOTID_BANNER_WEB        @"276395"
//test/0e89e16441e04df391f8cc10d6bdb2238584c3b7 268749

#define AD_APPLIPROMOTION_ID @"C2XKBXCW8GJ1Y0C5"

#define GOOGLE_ANALYTICS_TRACKING_ID @"UA-58146585-1"

#define HASH_TAG @"#買い物忘れ"

//TODO: 申請前に1にする
#define ENABLE_ANALYTICS 1
//TODO: 申請前に1にする
#define ENABLE_AD 1
//TODO: 申請前に、ウォール型広告のdebugフラグとIDを、AMoAdSettings.txtで確認する

#define APP_NAME @"もう買い物忘れしません"

#define KEY_COREDATA_CONVERT_FLG @"keyCoredataConvertFlg"
#define KEY_WALL_AD_SHOW_FLG     @"keyWallAdShowFlg"//FLG:YES = NOT in review
//#define KEY_REVIEW_DONE_FLG      @"keyReviewDoneFlg"//レビュー催促→インセンティブ制限　が解除されたフラグ
#define KEY_RADIUS_NOTIFI        @"keyRadiusNotification"//半径何メートル
#define KEY_NOTIFI_MSG           @"keyNotificationMsg"//通知メッセージ
#define KEY_ENABLE_CHANGE_NOTIFI_MSG @"keyEnableChangeNotifi"//このアプリをすでにレビュー済みなひと

#define APP_COLOR1 HEXCOLOR(0xfcfefc)//iconの背景

static BOOL const  NAVI_BAR_TRANSLUCENT = YES; //translucent
static CGFloat const AD_BANNER_HIGHT = 50.0f;
static int const IMAGE_COUNT = 3;
static CGFloat const AD_ICON_W = 32.0f;






