//
//  AppDelegate.h
//  Tokitama
//
//  Created by 鈴木治 on 2015/01/13.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

