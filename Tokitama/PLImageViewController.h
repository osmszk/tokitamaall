//
//  PLImageViewController.h
//  Tokitama
//
//  Created by 鈴木治 on 2015/01/21.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PLImageViewController : UIViewController

@property (nonatomic,weak) IBOutlet UIImageView *imageView;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *imageWidthConstraint;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint *imageHeightConstraint;

@end
