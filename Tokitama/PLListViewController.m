//
//  ViewController.m
//  Tokitama
//
//  Created by 鈴木治 on 2015/01/13.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import "PLListViewController.h"
#import "PLImageCollectionViewCell.h"

typedef NS_ENUM(NSInteger, PLArtworkViewTag){
    PLArtworkViewTagNone=0,
    PLArtworkViewTagBackgroundGrayView=1000,
    PLArtworkViewTagBackgroundArtworkExpandView=1001,
};

@interface PLListViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    CGRect _artworkImageBeforeRect;
    UIView *_bgGrayView;
    UIImageView *_expandedArtworkImageView;
}

@end

@implementation PLListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [UIView animateWithDuration:1.0f animations:^{
        self.logoBgView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [self.logoBgView removeFromSuperview];
        self.logoBgView = nil;
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom

- (void)removeExpandArtworkImageView
{
    [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        _expandedArtworkImageView.frame = _artworkImageBeforeRect;
        self.collectionView.alpha = 1.0f;
        
        _bgGrayView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        
        [_expandedArtworkImageView removeFromSuperview];
        _expandedArtworkImageView = nil;
        
        [_bgGrayView removeFromSuperview];
        _bgGrayView = nil;
        
    }];
}

#pragma mark - Touch Event

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    UITouch *touch = [touches anyObject];
    if(touch.view.tag == PLArtworkViewTagBackgroundGrayView){
        DEBUGLOG(@"bgview touch!");
        [self removeExpandArtworkImageView];
    }else if (touch.view.tag == PLArtworkViewTagBackgroundArtworkExpandView){
        DEBUGLOG(@"expandview touch!");
        [self removeExpandArtworkImageView];
        
        
    }
}

#pragma mark -

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 40;
}



- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PLImageCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"cell"
                                                                                       forIndexPath:indexPath];
    NSString *imageName = [NSString stringWithFormat:@"img01-%d",(int)indexPath.item];
    UIImage *image = [UIImage imageNamed:imageName];
    cell.kotobaImageView.image = image;
    return cell;
}

#pragma mark -

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger xIndex = indexPath.item%3;//0,1,2,3
    NSInteger yIndex = indexPath.item/3;//0,1,2,3,4,5...
    CGFloat offsetY = 0.0f;
    CGFloat y = yIndex*178+offsetY;
    y = y - self.collectionView.contentOffset.y;
    DEBUGLOG(@"offsetContent:%@",NSStringFromCGPoint(self.collectionView.contentOffset));
    
    CGFloat offsetX = ([PLUtil displaySize].width-3*120)/(3-1);
    CGRect beforeRect = CGRectMake(xIndex*(120+offsetX), y , 120, 178);
    _artworkImageBeforeRect = beforeRect;
    
    CGFloat wid = [PLUtil displaySize].width * 0.8f;
    CGFloat hight = 178.0f*wid/120.0f;
    CGRect afterRect = CGRectMake(([PLUtil displaySize].width-wid)/2, ([PLUtil displaySize].height-hight)/2, wid, hight);
    
    _bgGrayView = [[UIView alloc] initWithFrame:[PLUtil displayFrame]];
    _bgGrayView.backgroundColor = [UIColor blackColor];
    _bgGrayView.alpha = 0.0f;
    _bgGrayView.tag = PLArtworkViewTagBackgroundGrayView;
    _bgGrayView.userInteractionEnabled = YES;
    [self.view addSubview:_bgGrayView];
    
    _expandedArtworkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 300)];
    _expandedArtworkImageView.frame = beforeRect;
    _expandedArtworkImageView.tag = PLArtworkViewTagBackgroundArtworkExpandView;
    NSString *imageName = [NSString stringWithFormat:@"img01-%d",(int)indexPath.item];
    UIImage *image = [UIImage imageNamed:imageName];
    [_expandedArtworkImageView setImage:image];
    _expandedArtworkImageView.userInteractionEnabled = YES;
    [self.view addSubview:_expandedArtworkImageView];
    
    
    [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
        _expandedArtworkImageView.frame = afterRect;
        _bgGrayView.alpha = 0.7f;
    } completion:^(BOOL finished) {
    }];
}



@end
