//
//  PLTokitamaUtil.h
//  Tokitama
//
//  Created by 鈴木治 on 2015/01/19.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PLTokitamaUtil : NSObject

+ (NSArray *)pdfNames;

@end
