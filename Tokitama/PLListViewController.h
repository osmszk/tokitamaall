//
//  ViewController.h
//  Tokitama
//
//  Created by 鈴木治 on 2015/01/13.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PLListViewController : UIViewController

@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic,weak) IBOutlet UIView *logoBgView;

@end

