//
//  PLImageCollectionViewCell.h
//  Tokitama
//
//  Created by 鈴木治 on 2015/01/13.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PLImageCollectionViewCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIImageView *kotobaImageView;

@end
