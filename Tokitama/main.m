//
//  main.m
//  Tokitama
//
//  Created by 鈴木治 on 2015/01/13.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PLAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PLAppDelegate class]));
    }
}
