//
//  PLImageViewController.m
//  Tokitama
//
//  Created by 鈴木治 on 2015/01/21.
//  Copyright (c) 2015年 Plegineer Inc. All rights reserved.
//

#import "PLImageViewController.h"

@interface PLImageViewController ()

@end

@implementation PLImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSString *imageName = [self randomImageName];
    self.imageView.image = [UIImage imageNamed:imageName];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didComeBackFromBackground:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Notification

- (void)didComeBackFromBackground:(id)sender
{
    NSString *imageName = [self randomImageName];
    self.imageView.image = [UIImage imageNamed:imageName];

    
}

#pragma mark - Custom

- (NSString *)randomImageName
{
    int index = arc4random()%IMAGE_COUNT;
    NSString *file = [NSString stringWithFormat:@"tokitama%d.jpg",index+1];
    return file;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
